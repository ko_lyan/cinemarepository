using FilmsCatalog.Models;
using FilmsCore.Classes.DTOClasses;
using FilmsCore.Repository;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace TestProjectFilm
{
    [TestClass]
    public class UnitTest1
    {
        IFilmsRepository _films;
        [TestInitialize]
        public void Setup()
        {
            _films = new FilmsRepository(()=> { return null; });
        }

        [TestMethod]
        public void GetList()
        {

            var lst = _films.GetFilmsToTable(null,10,0);

            var lst2 = _films.GetFilmsToTable(null, 10, 10);

            CollectionAssert.AllItemsAreNotNull(lst2);
        }

        [TestMethod]
        public void SubmitForm()
        {
            EditFilm model = new EditFilm
            {
                edit_id = 0,
                edit_Name = "Blade1",
                edit_Description = "djfsdfsdfhskdfhkds"
            };
            _films.SubmitFilm(model);
        }

        [TestMethod]
        public void DubmitGuid()
        {
            _films.SubmitGuidFile("4cb5dd0e-8648-41e8-9e99-1170722a4066.jpg", 1);
        }
    }
}
