﻿using FilmsCore.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace FilmsCore.Repository
{
    public class AbstractBaseSettings
    {

        public CinemaContext _Context;
        public AbstractBaseSettings(Func<string> username)
        {
            _Context = new CinemaContext();
            _Context.UserName = username() ?? "";
        }
        //сравнение двух объектов по интерфейсу рефлексией
        public bool CompareTwoClassByInterface<TInterface, TEntity>(TEntity model1, TEntity model2) where TEntity : class, TInterface
        {

            var Imodel = typeof(TInterface);

            var props = Imodel.GetProperties()
                .Select(x => x.Name).ToList();


            foreach (var field in props)
            {

                PropertyInfo fld = typeof(TEntity).GetProperty(field);
                object val1 = fld.GetValue(model1);
                object val2 = fld.GetValue(model2);

                if (val1 == null && val2 == null)
                    continue;

                if ((val1 == null && val2 != null) || (val1 != null && val2 == null))
                    return false;

                if (!val1.Equals(val2))
                    return false;
            }
            return true;
        }

    }
}
