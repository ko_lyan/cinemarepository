﻿
using FilmsCatalog.Models;
using FilmsCore.Classes.Context;
using FilmsCore.Classes.DTOClasses;
using FilmsCore.Data;
using FilmsCore.DTOClasses;
using FilmsCore.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace FilmsCore.Repository
{
    public class FilmsRepository: AbstractBaseSettings, IFilmsRepository
    {
        public FilmsRepository(Func<string> conn) : base(conn)
        {

        }


        public List<TableFilm> GetFilmsToTable(Expression<Func<Film, bool>> expr = null, int take = 0, int skip = 0)
        {
            
            var lst = _Context.Films.AsQueryable();
            
            if (expr != null)
            {
                lst = lst.Where(expr).AsQueryable();
            }

            var films = lst
                .Skip(skip)
                .Take(take)
                
                .Select(x => new TableFilm 
                { 
                    id = x.id, 
                    Name = x.Name, 
                    Description = x.Description ,
                    guid_poster = x.poster,
                    username = x.user_in
                })
                .ToList();

            return films;
        }

        public EditFilm GetForm(int id)
        {
            if (id==0)
            {
                return new EditFilm();
            }
            var film = _Context.Films.Where(x => x.id == id).FirstOrDefault();

            if (film == null)
                throw new Exception("Film NotFound");
            //тут можно автомаппер, но полей мало так что так удобнее.
            var form = new EditFilm
            {
                edit_id = film.id,
                edit_Name = film.Name,
                edit_Description = film.Description,
                edit_year = film.year,
                edit_produser = film.produser,
                guid_poster = film.poster
            };

            return form;
        }

        public int SubmitFilm(EditFilm film)
        {
            Film model = new Film
            {
                id = film.edit_id,
                Name = film.edit_Name?.Trim(),
                Description = film.edit_Description?.Trim(),
                year = film.edit_year,
                produser=film.edit_produser
            };

            if (model.id == 0)//если ид==0 - тогда создание
            {
                 
                _Context.Films.Add(model) ;
                _Context.SaveChanges();
                return model.id;
            }
            else// если обновление
            {
                bool formUpdate = false;
                //забираем модель из базы, и делаем неотслеживаемой
                var change_detalies = _Context.Films.Where(x => x.id == model.id).FirstOrDefault();
                _Context.Entry(change_detalies).State = EntityState.Detached;
                //сравниваем изменяемые поля
                formUpdate = CompareTwoClassByInterface<IGeneralField, Film>(model, change_detalies);

                //если поля изменились обновляем
                if (!formUpdate)
                    _Context.UpdateFromInterface<Film, IGeneralField>(model);
                else
                    return model.id;//

                _Context.SaveChanges();
                return model.id;
            }
            //применяем все действия 

        }

        public void SubmitGuidFile(string poster_guid, int id_films )
        {
            Film model = new Film()
            {
                id = id_films,
                poster = poster_guid
            };
            _Context.UpdateFromInterface<Film, IGuidFilm>(model);
            _Context.SaveChanges();

        }
        public long FilmsCount()
        {
            return _Context.settings.FirstOrDefault()?.films_count??1;
        }
    }

    public interface IFilmsRepository
    {
        //получение фильмов
        List<TableFilm> GetFilmsToTable(Expression<Func<Film, bool>> expr = null, int take = 0, int skip = 0);
        //получение формы
        EditFilm GetForm(int id);
        //сохранение формы
        int SubmitFilm(EditFilm film);
        //сохранение постера
        void SubmitGuidFile(string poster_guid, int id_films);
        //количество фильмов
        long FilmsCount();
    }
}
