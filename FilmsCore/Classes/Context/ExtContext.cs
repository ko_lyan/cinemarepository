﻿using FilmsCore.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FilmsCore.Classes.Context
{
    public static class Ext
    {

        public static void UpdateFromInterface<TEntity, TInterface>(this CinemaContext _Context, TEntity model)
where TEntity : class, TInterface

        {
            var Imodel = typeof(TInterface);

            var props = Imodel.GetProperties()
                .Select(x => x.Name).ToList();
            try
            {

                if (!_Context.Set<TEntity>().Local.Any(e => e == model))
                    _Context.Set<TEntity>().Attach(model);

                foreach (var field in props)
                {

                    _Context.Entry(model).Property(field).IsModified = true;
                }

            }
            catch (Exception ex)
            {

            }
        }

    }
}
