﻿using FilmsCatalog.Models;
using FilmsCore.Interface;
using FilmsCore.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Linq;

namespace FilmsCore.Data
{
    public class CinemaContext : DbContext
    {
        public string UserName { get; set; }

        public DbSet<Film> Films { get; set; }
        public DbSet<settings> settings { get; set; }


        public CinemaContext()
        {
            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var builder = new ConfigurationBuilder()
    .AddJsonFile("appsettings.json");
            IConfiguration AppConfiguration = builder.Build();
            optionsBuilder.UseSqlServer(AppConfiguration.GetConnectionString("DefaultConnection"));

        }

        public override int SaveChanges()
        {
            var i_create = ChangeTracker.Entries<ICreate>();

            if (i_create != null)
            {
                DateTime this_time = DateTime.Now;
                foreach (var entry in i_create.Where(c => c.State != EntityState.Unchanged))
                {

                    if (entry.Entity.id == 0)
                    {
                         
                        entry.Entity.user_in = UserName;

                    }
                }
            }

            return base.SaveChanges();
        }
    }
}
