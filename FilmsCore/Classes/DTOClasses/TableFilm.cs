﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

//DTO классы, чтоб не использовать модели
namespace FilmsCore.DTOClasses
{

    //класс для отрисовки главной таблицы
    public class TableFilm
    {
        public int id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string guid_poster { get; set; }
        public string username { get; set; }
    }
}
