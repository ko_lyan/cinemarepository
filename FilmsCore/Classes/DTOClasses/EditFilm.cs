﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FilmsCore.Classes.DTOClasses
{
    //редактирование формы
    public class EditFilm
    {
        public int edit_id { get; set; }
        public string edit_Name { get; set; }
        public string edit_Description { get; set; }
        public int? edit_year { get; set; }
        public string edit_produser { get; set; }
        public string guid_poster {get;set;}
    }
}
