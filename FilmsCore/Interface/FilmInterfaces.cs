﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FilmsCore.Interface
{
    public interface IFilmId
    {
        int id { get; set; }
    }
    //основная форма
    public interface IGeneralField: IFilmId
    {
        string Name { get; set; }
        string Description { get; set; }
        int? year { get; set; }

        string produser { get; set; }
    }
    //для сохранение постера
    public interface IGuidFilm : IFilmId
    {
        string poster { get; set; }
    }
    //интерфейс для заполнения пользователя в user_in
    public interface ICreate : IFilmId
    {
        string user_in { get; set; }
    }


}
