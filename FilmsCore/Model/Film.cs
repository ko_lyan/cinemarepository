﻿using FilmsCore.Interface;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FilmsCatalog.Models
{
    public class Film: IGeneralField,  IFilmId, IGuidFilm, ICreate
    {
        [Key]
        public int id { get; set; }
        public string Name { get; set; }

        public string Description { get; set; }

        public int? year { get; set; }

        public string produser { get; set; }

        public string poster { get; set; }

        public string user_in { get; set; }

    }
}
