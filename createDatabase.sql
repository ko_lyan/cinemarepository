USE [aspnet-FilmsCatalog-0248253E-E432-4B2A-B3BF-C06124FFFBA2]
GO

/****** Object:  Table [dbo].[Films]    Script Date: 23.06.2021 14:28:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Films](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](250) NULL,
	[Description] [varchar](5000) NULL,
	[year] [int] NULL,
	[produser] [varchar](250) NULL,
	[poster] [varchar](50) NULL,
	[user_in] [varchar](250) NULL,
 CONSTRAINT [PK_Films] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


USE [aspnet-FilmsCatalog-0248253E-E432-4B2A-B3BF-C06124FFFBA2]
GO

/****** Object:  Trigger [dbo].[Films_Update]    Script Date: 23.06.2021 14:28:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


Create TRIGGER [dbo].[Films_Update]
ON [dbo].[Films]
AFTER INSERT
AS

Update settings set [films_count] = isnull([films_count],0) +
(select count(*)
FROM INSERTED)

Update settings set [films_count] = isnull([films_count],0) -
(select count(*)
FROM deleted)
GO

ALTER TABLE [dbo].[Films] ENABLE TRIGGER [Films_Update]
GO

USE [aspnet-FilmsCatalog-0248253E-E432-4B2A-B3BF-C06124FFFBA2]
GO

/****** Object:  Table [dbo].[settings]    Script Date: 23.06.2021 14:28:52 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[settings](
	[films_count] [bigint] NOT NULL,
 CONSTRAINT [PK_settings] PRIMARY KEY CLUSTERED 
(
	[films_count] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO



