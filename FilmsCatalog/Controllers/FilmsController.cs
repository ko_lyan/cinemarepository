﻿using FilmsCore.Classes.DTOClasses;
using FilmsCore.Repository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace FilmsCatalog.Controllers
{
    public class FilmsController : Controller
    {
        IFilmsRepository _film;
        private IWebHostEnvironment _hostingEnvironment;

        public FilmsController(IFilmsRepository film, IWebHostEnvironment environment)
        {
            _film = film;
            _hostingEnvironment = environment;
        }
        [Authorize]
        public IActionResult List()
        {
            decimal count_pages = (decimal)_film.FilmsCount() / 10;
            ViewBag.CountPages = Math.Ceiling(count_pages);
            return View();
        }

        public IActionResult DebugForm()
        {
            return View();
        }

        [HttpPost]
        public  JsonResult SubmitFilm(EditFilm film)
        {
             
            // если есть лшибки - возвращаем false
            if (!ModelState.IsValid)
                return Json(false);

            try
            {
                return Json(_film.SubmitFilm(film));
            }
            catch
            {
                return Json(-1);
            }
        }
        [HttpPost]
        public IActionResult AddFile( IFormFile file, int id_film)
        {
            Guid file_guid = Guid.NewGuid();
            string fileName = System.IO.Path.GetFileName(file.FileName);
            string ext = System.IO.Path.GetExtension(file.FileName);
            string uploads = Path.Combine(_hostingEnvironment.WebRootPath, "Files");
        
                if (file.Length > 0)
                {

                    string filePath = Path.Combine(uploads, file_guid.ToString()+ ext) ;
                    using (Stream fileStream = new FileStream(filePath, FileMode.Create))
                    {
                        file.CopyToAsync(fileStream);
                        _film.SubmitGuidFile(file_guid.ToString() + ext, id_film);

                    }
                }
            return View();
        }
        public IActionResult FormFilm(int id)
        {
            return ViewComponent("FormFilm", new { id = id });
        }

        public IActionResult FilmList(string find, int page)
        {
            return ViewComponent("FilmList", new {find = find, page = page-1 });
        }

        public IActionResult Pagination(bool is_searh = true)
        {
            return ViewComponent("Pagination", new { is_searh = is_searh });
        }
        // о
    }
}
