﻿
using FilmsCatalog.Models;
using FilmsCore.Repository;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;


namespace FilmListCatalog.Components
{

    public class FilmListViewComponent : ViewComponent
    {
        IFilmsRepository _film;
        public FilmListViewComponent(IFilmsRepository film)
        {
            _film = film;
        }
        public IViewComponentResult Invoke(string find, int page=0)
        {
            Expression<Func<Film, bool>> expr = null;

            if (!string.IsNullOrWhiteSpace(find))
            {
                expr = m =>  m.Name.Contains(find) || m.Description.Contains(find); 
            }
            return View(_film.GetFilmsToTable(expr, 10,page*10));
        }
    }
}
