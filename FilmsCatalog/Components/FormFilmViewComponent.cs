﻿using FilmsCore.Repository;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FilmsCatalog.Components
{
    public class FormFilmViewComponent : ViewComponent
    {
        IFilmsRepository _film;
        public FormFilmViewComponent(IFilmsRepository film)
        {
            _film = film;
        }

        public IViewComponentResult Invoke(int id)
        {


            return View(_film.GetForm(id));
        }
    }
}
