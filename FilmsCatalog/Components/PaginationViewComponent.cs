﻿using FilmsCore.Repository;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FilmsCatalog.Components
{
    public class PaginationViewComponent : ViewComponent
    {
        IFilmsRepository _film;
        public PaginationViewComponent(IFilmsRepository film)
        {
            _film = film;
        }

        public IViewComponentResult Invoke(bool is_searh=true)
        {

            return View();
        }
    }
}
