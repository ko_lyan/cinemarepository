﻿$(document).ready(function () {
    //$('#myModalFilmForm').on('show.bs.modal', function (e) {
    init_pagination();
    //});
    //InitDatatable()

});
//инициализация жквери плагина пагинации
function init_pagination() {
    $('#films_padding').pagination({
        total: count_pages,  
        current: 1, 
        length: 1,  
        size: 2,  
        prev: 'Previous',
        next: 'Next',

        click: function (options, $target) { 
            current_page = options.current;
            PerformCallbackTable();
            //console.log(options);
            //$target.next(".show").text('Current: ' + options.current);
        }
    });
}

var current_page = 1;
//думал плагин даатэйбл использовать, но передумал
//function InitDatatable() {
//    $('#filmsTable').DataTable({
//        paging: false,
//        "columnDefs": [
//            //{
//            //    "targets": [0],
//            //    "visible": false,
//            //}
//        ]
//    });
//}
//сохранение данных формы
function submitfilm()
{
    var user_form_s = $("#formFilm").serialize();



    var link = '/Films/SubmitFilm'
    $.ajax({
        url: link,//getBaseUrl("Films") + '/SubmitFilm',
        type: 'POST',
        data: user_form_s,
        success: function (data) {
            $('#myModalFilmForm').modal('hide');
            //добавление постера
            add_poster(data);
            //PerformCallbackTable();
        },
        error: function () {
            console.log('Ошибка во время отправки комментария', this);
        }
    });

    if (true) {

    }
}

function add_poster(id_film)
{
    var formdata2 = new FormData();
    var input = document.getElementById('uploadedFile');
    var files = input.files;
    if (input.files.length == 0)
    {
        //если постер не выбран обновляем таблицу
        PerformCallbackTable();
        return;
    }
    formdata2.append('file', files[0]);
    //formdata2.append('id_film', $('#edit_id').val());
    formdata2.append('id_film', id_film);

    $.ajax(
        {
            url: "/Films/AddFile",
            data: formdata2,
            async: false,
            processData: false,
            contentType: false,
            type: "POST"
            //success: function (data) {
               
            //}
        }
    );
    //обновляем таблицу после загрузки постера
    PerformCallbackTable();
}
var m_film_id;
//открытие формы
function openDetalies(film_id)
{
    m_film_id = film_id;
    $('#FilmFormDiv').html('');
    $('#myModalFilmForm').modal(); 
    //window.setInterval(refreshComponent, 1000);
    $.get("/Films/FormFilm", { id: m_film_id }, function (data) { $('#FilmFormDiv').html(data); });


}


function PerformCallbackTable() {
    //поиск 
    var find = $('#find_txt').val().trim();
    //каллбэк таблицы
    $.get("/Films/FilmList", { find: find, page: current_page }, function (data) { $('#table_films').html(data); });

}
////поиск
function searh_text() {
    var is_searh = false;
    current_page = 1;
    var find = $('#find_txt').val().trim();

    if (find != '') {

        is_searh = true;
    }

    if (is_searh) {

        $('#pagination_div').html('');
        init_pagination();
    }
    else {
        $.get("/Films/Pagination", { is_searh: is_searh }, function (data) {
            $('#pagination_div').html(data);
            init_pagination();
        });
    }
    PerformCallbackTable(); 
    
}
//очистка поиска
function ClearSearh()
{
    $('#find_txt').val('');
    searh_text();
}